class HumanPlayer
  require 'byebug'
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Enter move where form is 0, 0"
    input = gets.chomp.split(",")

    x = input.first.strip.to_i
    y = input.last.strip.to_i

    [x, y]
  end

  def display(board)
    grid = board.grid
    puts " #{grid[2][0] || " "} | #{grid[2][1] || " "} | #{grid[2][2] || " "}"
    puts "---+---+---"
    puts " #{grid[1][0] || " "} | #{grid[1][1] || " "} | #{grid[1][2] || " "}"
    puts "---+---+---"
    puts " #{grid[0][0] || " "} | #{grid[0][1] || " "} | #{grid[0][2] || " "}"
  end

end
