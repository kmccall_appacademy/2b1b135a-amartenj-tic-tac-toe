class Board
  require 'byebug'
  attr_accessor :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos.first][pos.last] = mark
  end

  def empty?(pos)
    @grid[pos.first][pos.last].nil?
  end

  def winner
    symbols = %i(X O)

    symbols.each do |sym|
      return sym if
        # rows
        [@grid[0][0], @grid[0][1], @grid[0][2]] == [sym] * 3 ||
        [@grid[1][0], @grid[1][1], @grid[1][2]] == [sym] * 3 ||
        [@grid[2][0], @grid[2][1], @grid[2][2]] == [sym] * 3 ||

        # columns
        [@grid[0][0], @grid[1][0], @grid[2][0]] == [sym] * 3 ||
        [@grid[0][1], @grid[1][1], @grid[2][1]] == [sym] * 3 ||
        [@grid[0][2], @grid[1][2], @grid[2][2]] == [sym] * 3 ||

        #diagonal
        [@grid[0][0], @grid[1][1], @grid[2][2]] == [sym] * 3 ||
        [@grid[0][2], @grid[1][1], @grid[2][0]] == [sym] * 3
    end

    nil
  end

  def over?
    !winner.nil? || @grid.flatten.count(nil) == 0
  end

end
