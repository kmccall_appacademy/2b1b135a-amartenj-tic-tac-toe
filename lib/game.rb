require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :player_one
  attr_accessor :player_two
  attr_reader :current_player
  attr_accessor :board

  def initialize(player_one, player_two)
    @current_player = @player_one = player_one
    @player_two = player_two

    @player_one.mark = :X
    @player_two.mark = :O

    @board = Board.new
  end

  def play
    while !@board.over?
      @current_player.display(@board)
      play_turn
    end

    @player_one.display(@board)
    result = @board.winner.nil? ? "Cats Game" : "Winner is #{@board.winner}"
    puts "Game Over. #{result}"
  end

  def switch_players!
    @current_player = (@current_player == @player_one ? @player_two : @player_one)
  end

  def play_turn
    move = @current_player.get_move
    @board.place_mark(move, @current_player.mark)
    switch_players!
  end

end

if __FILE__ == $PROGRAM_NAME
  player_one = HumanPlayer.new("Adam")
  player_two = ComputerPlayer.new("Charlie")

  game = Game.new(player_one, player_two)
  game.play
end
