class ComputerPlayer
  require 'byebug'
  attr_reader :name
  attr_reader :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    moves = []

    @board.grid.each_with_index do |row, r_idx|
      row.each_with_index do |cell, c_idx|
        moves << [r_idx, c_idx] if cell == nil
      end
    end

    # p moves

    moves.each do |move|
      @board.place_mark(move, @mark)

      if @board.winner == @mark
        @board.place_mark(move, nil)
        return move
      end

      @board.place_mark(move, nil)
    end

    moves.shuffle.first
  end

  def display(board)
    @board = board
  end

end
